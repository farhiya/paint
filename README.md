# Install paint.js

Instructions to install and run paint js on an ubuntu 20.04 machine

## Pre-requisite
- have an ubuntu machine running
    - security group cohort9-homeips
- have ch9_shared key

## Instructions
- Clone this repo
- Change dircectory to paint (`cd paint`)
- Run `./paint_install_ubuntu.sh` on the terminal
    - There shouldn't be any permission issue but if there is run `chmod +x paint_install_ubuntu.sh` then run `./paint_install_ubuntu.sh`
- You will be prompted to input your ip, enter ubuntu machine public ip and press enter
- Commands needed to install and run paint js are executed automatically (commands are in 'paint_install_ubuntu.sh' file
- This will take a few minutes, and the terminal will show a line starting with `CSS change detected `. Installation is now complete
- Go to http://<your-machine-ip>:1999 
- Enjoy paint


# Creating a Bastion host

Instructions on how to create a Bastion host for paint js


## Instructions

### Creating bastion-host

- Create an ubuntu machines called bastion-host
    - security groups cohort9-homeips or a security group with your ip address has access
    - have ch9_shared key

- Created a security group (farhiya-bastion) added an inbound rule where type=ssh, source=custom and in the box next to source enter the private ip of the bastion host machine
  
- Added the security group farhiya-bastion to farhiya-paint ubuntu machine so paint can be acessed on web browser and through the bastion host machin


### Managing ssh

Configuring ssh agent:
- `ssh-add -K <key-path/your-key.pem` ----> ssh-add -K ~/.ssh/ch9_shared.pem


Connecting to the bastion host with ssh-agent
- `ssh-A ubuntu@<Public IPv4 DNS>`  -----> `ssh -A ubuntu@ec2-54-246-169-157.eu-west-1.compute.amazonaws.com`
    - Public IPv4 DNS can be found on your instance in the 'Details' tab at the top of the third coloumn


Log in to farhiya-paint through bastion-host
- `ssh ubuntu@<>farhiya-paint-internal-ip` ----->  `ssh ubuntu@172.31.44.193`
- run `(cd ~/jspaint-master; npm run dev)` to start paint js

