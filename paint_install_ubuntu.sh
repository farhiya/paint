echo "Please enter your ip address: "

read ip_address

COUNTER=0
for arg in $ip_address
do
COUNTER=$(expr $COUNTER + 1)
done

if ((COUNTER == 1))
then 
    echo "This is your ip $arg" 

elif ((COUNTER < 1))
then
    echo "There was no input. Try again! "
    exit 1
elif ((COUNTER > 1))
then

    echo "That was too many inputs. Try again! "
    exit 1

fi

# opening ubuntu and removing fingerprint check at given ip
ssh -o StrictHostKeyChecking=no -i ~/.ssh/ch9_shared.pem ubuntu@$ip_address '

# update all packages
sudo apt update

# install the follow packages
sudo apt install nodejs -y
sudo apt install -g eslint
sudo apt install npm -y
sudo apt install npm@latest
sudo apt install unzip

# download the git repoas a zip file
wget https://github.com/1j01/jspaint/archive/refs/heads/master.zip

# unzip the repo
unzip master.zip

# fix any vulenarbilities (updates some packages being used in the repo)
(cd ~/jspaint-master; npm audit fix) 


# run
(cd ~/jspaint-master; npm run dev)
'
